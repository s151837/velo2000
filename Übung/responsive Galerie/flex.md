# Wirkung von CSS

Wir haben die Schriftart der Überschrift geändert
```css
.headline {
  font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
}
```

Jetzt müssen die Bilder nich eine einheitliche Größe und einen Rahmen bekommen, damit es besser aussieht
```css
img {
    width:400px;
    margin:10px;
}
```
Außerdem sollen die Bilder sich an die Größe des Bildschirms anpassen, was mit Flex funktioniert
```css
.container {
    display:flex;
    flex-wrap:wrap;
    margin:20px;
}
```